<?php
/**
 * Plugin Name: Kavel module
 * Plugin URI: http://www.heerenveen.nl/plugins/kavel-module/
 * Description: Plugin voor het weergeven van verschillende kavels
 * Version: 1.0.20
 * Author: Gemeente Heerenveen
 * Author URI: https://www.heerenveen.nl
 * Requires at least: 3.0
 * Tested up to: 4.7.3
 * License URI: https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * License: EUPL v1.2
 * 
 * Copyright 2020 Gemeente Heerenveen
 * 
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
if ( ! defined('GH_LOT_MODULE_VERSION')) define('GH_LOT_MODULE_VERSION', '1.0.20');
if ( ! defined('GH_LOT_MODULE_FILE')) define('GH_LOT_MODULE_FILE', __FILE__);
if ( ! defined('GH_LOT_MODULE_DIR')) define('GH_LOT_MODULE_DIR', dirname(__FILE__));
if ( ! defined('GH_LOT_MODULE_URL')) define('GH_LOT_MODULE_URL', plugins_url( basename(GH_LOT_MODULE_DIR) ));

/**
 * Main Plugin Class
 *
 * @class GhKavelModule
 * @version  1.1.0
 */
class GhLotModule {
	public function __construct() {
		include GH_LOT_MODULE_DIR . '/lib/helpers.php';

		include GH_LOT_MODULE_DIR . '/classes/class-gh-lot-updater.php';

		include GH_LOT_MODULE_DIR . '/classes/class-gh-lot-init.php';

		include GH_LOT_MODULE_DIR . '/classes/class-gh-lot-posttype.php';

		include GH_LOT_MODULE_DIR . '/classes/class-gh-lot-fields.php';
		
		include GH_LOT_MODULE_DIR . '/classes/class-gh-lot-templates.php';
	}
}
new GhLotModule();