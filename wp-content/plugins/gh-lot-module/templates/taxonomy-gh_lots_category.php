<?php get_header(); ?>
	<?php 
		$term_id 			= get_queried_object_id(); 
		$term_description 	= term_description();
	?>
	<div class="container wrap">
		<?php
			echo ($term_description && $term_description !== '' ? do_shortcode( $term_description ) : ''); 

			switch ( gh_lot_pagetype($term_id) ) {
				case 'lot':
					gh_lot_map($term_id, true, true);
					break;
				case 'overview':
					gh_lot_overview($term_id, true);
					break;
				default:
					break;
			}
		?>
	</div>
<?php get_footer(); ?>