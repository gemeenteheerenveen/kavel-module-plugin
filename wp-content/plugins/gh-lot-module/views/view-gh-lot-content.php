<?php global $post; ?>
<div class="gh_lot_map--container">
	<div class="gh_lot_map--top_filters">
		<span><?php _e('Filteren op:'); ?></span>
		<ul class="gh_lot_map--filter_1">
			<li>
				<select name="gh_lot_price_filter" id="gh_lot_price_filter" aria-label="Maximale prijs">
					<option value="">-- Maximale prijs --</option>
					<option value="50000">&euro; 50.000</option>
					<option value="100000">&euro; 100.000</option>
					<option value="150000">&euro; 150.000</option>
					<option value="200000">&euro; 200.000</option>
				</select>
			</li>
			<li>
				<select name="gh_lot_size_filter" id="gh_lot_size_filter" aria-label="Oppervlakte">
					<option value="">-- Oppervlakte --</option>
					<option value="0-100">0 m² - 100 m²</option>
					<option value="100-200">100 m² - 200 m²</option>
					<option value="200-300">200 m² - 300 m²</option>
					<option value="400-400">300 m² - 400 m²</option>
					<option value="400-500">400 m² - 500 m²</option>
					<option value="500-600">500 m² - 600 m²</option>
					<option value="600-700">600 m² - 700 m²</option>
					<option value="700">700 m² en groter</option>
				</select>
			</li>
			<li>
				<a href="#" class="gh_lot_map--reset">reset filters</a>
			</li>
		</ul>
		<ul class="gh_lot_map--filter_2">
			<li>
				<a href="#" class="gh_lot_map--btn for_sale"><?php echo gh_lot_count($term_id, 'for_sale'); ?> Te koop</a>
			</li>
			<li>
				<a href="#" class="gh_lot_map--btn optional"><?php echo gh_lot_count($term_id, 'optional'); ?> In optie</a>
			</li>
			<li>
				<a href="#" class="gh_lot_map--btn sold"><?php echo gh_lot_count($term_id, 'sold'); ?> Verkocht</a>
			</li>
		</ul>
	</div>
	<div id="gh_lot_map--wrapper">
	    <img src="<?php echo gh_lot_image_url($term_id); ?>" usemap="#gh_lot_map" class="gh_lot_map--img" alt="Kavelkaart">
	</div>

	<?php if ( have_posts() ) : ?>
	    <map name="gh_lot_map" id="gh_lot_map">
	    	<?php while ( have_posts() ) : the_post();
	    		$gh_lot_location 	= get_post_meta( $post->ID, 'gh_lot_location', true );
	    		$gh_lot_status 		= get_post_meta( $post->ID, 'gh_lot_status', true );
	    		$gh_lot_price 		= get_post_meta( $post->ID, 'gh_lot_price', true );
	    		$gh_lot_size 		= get_post_meta( $post->ID, 'gh_lot_size', true );
	    		?>
	    		<area shape="poly" coords="<?php echo $gh_lot_location; ?>" href="javascript:void();" class="gh_lot--status_<?php echo $gh_lot_status; ?>" alt="<?php echo $post->ID; ?>" title="<?php echo gh_lot_title_html($post->ID); ?>" data-status="<?php echo $gh_lot_status; ?>" data-price="<?php echo $gh_lot_price; ?>" data-size="<?php echo $gh_lot_size; ?>" data-unique-id="<?php echo $post->ID; ?>" <?php echo gh_lot_color_html($post->ID, $gh_lot_status); ?>>
	    	<?php endwhile; ?>
	    </map>
	<?php endif; ?>
</div>
<?php if ( have_posts() ) : ?>
	<div class="gh_lot_map--price_table">
		<h3>Prijslijst</h3>
		<table>
			<thead>
				<tr>
					<th>Kavelnummer</th>
					<th>Status</th>
					<th>Oppervlakte</th>
					<th>Prijs</th>
					<th>Kavelpaspoort</th>
				</tr>
			</thead>
			<tbody>
				<?php while ( have_posts() ) : the_post();
					$gh_lot_number 	= get_post_meta( $post->ID, 'gh_lot_number', true );
	    			$gh_lot_status 	= get_post_meta( $post->ID, 'gh_lot_status', true );
	    			$gh_lot_price 	= get_post_meta( $post->ID, 'gh_lot_price', true );
	    			$gh_lot_size 	= get_post_meta( $post->ID, 'gh_lot_size', true );
	    			?>
					<tr class="gh_lot_map--price_status <?php echo $gh_lot_status; ?>" id="gh_lot_anchor--<?php echo $post->ID; ?>">
						<td class="first-child"><?php echo $gh_lot_number; ?></td>
						<td><?php echo gh_lot_format_status($gh_lot_status); ?></td>
						<td><?php echo gh_lot_format_size($gh_lot_size); ?></td>
						<td><?php echo gh_lot_format_price($gh_lot_price); ?></td>
						<td>
							<?php echo gh_lot_download_link($post->ID); ?>
						</td>
					</tr>
				<?php endwhile; ?>
			</tbody>
		</table>
	</div>
<?php endif; ?>