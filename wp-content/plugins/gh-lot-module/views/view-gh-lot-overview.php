<div class="gh_lot_overview--container">
	<?php if( $show_image ) { $image_url = gh_lot_image_url($term_id, 'full'); ?>
		<?php echo ($image_url !== '' ? '<img src="'.$image_url.'" class="gh_lot_overview--image" alt="">' : ''); ?>
	<?php } ?>
	<hr>
	<?php foreach (gh_lot_term_children($term_id) as $key => $term) { ?>
		<a href="<?php echo get_term_link($term); ?>"><?php echo $term->name; ?></a><br/>
	<?php } ?>
</div>