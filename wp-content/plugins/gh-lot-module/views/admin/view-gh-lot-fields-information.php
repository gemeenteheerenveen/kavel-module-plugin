<table class="form-table table-bordered">
	<tbody>
		<tr valign="top">
			<th>
				<label>Status</label>
			</th>
			<td>
				<select name="gh_lot_status" class="chosen-select">
					<option value="">- Selecteer een status -</option>
					<option value="for_sale" <?php selected($gh_lot_status, 'for_sale', true); ?>>Te koop</option>
					<option value="optional" <?php selected($gh_lot_status, 'optional', true); ?>>In optie</option>
					<option value="sold" <?php selected($gh_lot_status, 'sold', true); ?>>Verkocht</option>
				</select>
			</td>
		</tr>
		<tr valign="top">
			<th>
				<label>Kavelnummer</label>
			</th>
			<td>
				<input type="text" name="gh_lot_number" value="<?php echo $gh_lot_number; ?>" class="regular-text">
			</td>
		</tr>
		<tr valign="top">
			<th>
				<label>Oppervlakte</label>
			</th>
			<td>
				<input type="text" name="gh_lot_size" value="<?php echo $gh_lot_size; ?>" class="regular-text">
			</td>
		</tr>
		<tr valign="top">
			<th>
				<label>Prijs</label>
			</th>
			<td>
				<input type="text" name="gh_lot_price" value="<?php echo $gh_lot_price; ?>" class="regular-text">
			</td>
		</tr>
		<tr valign="top">
			<th>
				<label>Kavelpaspoort</label>
			</th>
			<td>
				<div class="gh_lot_image-preview-wrapper">
					<img id="gh_lot_image-preview" src="<?php echo $attachment_thumb; ?>" title="<?php echo $attachment_title; ?>">
				</div>
				<input id="upload_gh_lot_image_button" type="button" class="button" value="<?php _e('Selecteer een document', 'gh-lot-module'); ?>" />
				<input id="upload_gh_lot_image_remove" type="button" class="button button-primary" value="<?php _e('Verwijder document', 'gh-lot-module'); ?>" />
				<input type="hidden" name="gh_lot_document" id="gh_lot_image" value="<?php echo $gh_lot_document; ?>">
			</td>
		</tr>
	</tbody>
</table>