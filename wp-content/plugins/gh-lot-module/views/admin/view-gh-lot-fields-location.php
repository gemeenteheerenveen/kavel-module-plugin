<div class="gh_drawing--container">
	<ul class="gh_drawing--tools">
		<li>
			<a href="#" class="gh_tool--drawing">
				<i class="fa fa-paint-brush" aria-hidden="true"></i>
			</a>
		</li>
		<li>
			<a href="#" class="gh_tool--moving">
				<i class="fa fa-arrows" aria-hidden="true"></i>
			</a>
		</li>
		<li>
			<a href="#" class="gh_tool--zoom_in">
				<i class="fa fa-search-plus" aria-hidden="true"></i>
			</a>
		</li>
		<li>
			<a href="#" class="gh_tool--zoom_out">
				<i class="fa fa-search-minus" aria-hidden="true"></i>
			</a>
		</li>
		<li>
			<a href="#" class="gh_tool--reset">
				<i class="fa fa-refresh" aria-hidden="true"></i>
			</a>
		</li>
	</ul>
	<section id="pan-when-zoomed">
		<div class="parent">
			<div class="panzoom">
				<textarea name="gh_lot_location" class="gh_drawing--canvas" data-image-url="<?php echo $attachment_url; ?>" style="display:none;"><?php echo $gh_lot_location; ?></textarea>
			</div>
		</div>
	</section>
</div>