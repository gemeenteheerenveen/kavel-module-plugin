<tr class="form-field">  
	<th scope="row" valign="top">  
		<label for="gh_lot_image-btn"><?php _e('Pagina type', 'gh-lot-module'); ?></label>  
	</th>  
	<td>
		<select name="term_meta[gh_lot_page]" id="gh_lot_page" class="postform">
			<option value=""><?php _e('Standaard', 'gh-lot-module'); ?></option>
			<option value="lot" <?php selected($gh_lot_page, 'lot', true); ?>><?php _e('Kavel pagina', 'gh-lot-module'); ?></option>
			<option value="overview" <?php selected($gh_lot_page, 'overview', true); ?>><?php _e('Overzicht pagina', 'gh-lot-module'); ?></option>
		</select>
	</td>  
</tr>
<tr class="form-field">  
	<th scope="row" valign="top">  
		<label for="gh_lot_image-btn"><?php _e('Achtergrond', 'gh-lot-module'); ?></label>  
	</th>  
	<td>
		<div class="gh_lot_image-preview-wrapper">
			<img id="gh_lot_image-preview" src="<?php echo $attachment_thumb; ?>">
		</div>
		<input id="upload_gh_lot_image_button" type="button" class="button" value="<?php _e('Selecteer een kaart', 'gh-lot-module'); ?>" />
		<input id="upload_gh_lot_image_remove" type="button" class="button button-primary" value="<?php _e('Verwijder kaart', 'gh-lot-module'); ?>" />
		<input type="hidden" name="term_meta[gh_lot_image]" id="gh_lot_image" value="<?php echo $attachment_id; ?>">
	</td>  
</tr>