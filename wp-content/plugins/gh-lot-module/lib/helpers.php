<?php
/*
* Copyright 2020 Gemeente Heerenveen
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
*/
	function gh_lot_map($term_id, $show_filters = false, $show_table = false) {
		include GH_LOT_MODULE_DIR . '/views/view-gh-lot-content.php';
	}

	function gh_lot_overview($term_id, $show_image = false) {
		include GH_LOT_MODULE_DIR . '/views/view-gh-lot-overview.php';
	}

	function gh_lot_image_url($term_id, $size = 'full') {
		$term_meta 			= get_option( "taxonomy_term_$term_id" );
		$attachment_id 		= (isset($term_meta['gh_lot_image']) && $term_meta['gh_lot_image'] ? $term_meta['gh_lot_image'] : '');
		$attachment_url 	= ($attachment_id !== '' ? wp_get_attachment_image_url( $attachment_id, $size ) : '');

		return $attachment_url;
	}

	function gh_lot_term_has_children($term_id) {
		$term_children = get_term_children($term_id, 'gh_lots_category');

		return ($term_children ? true : false);
	}

	function gh_lot_term_children($term_id) {
		$term_children 		= array();
		$term_children_ids 	= get_term_children($term_id, 'gh_lots_category');

		if( $term_children_ids ) {
			foreach ($term_children_ids as $term_child_id) {
				$term_children[] = get_term_by( 'id', $term_child_id, 'gh_lots_category' );
			}
		}

		return $term_children;
	}

	function gh_lot_count($term_id, $status) {
		global $wpdb;
		$count = $wpdb->get_var( "SELECT COUNT($wpdb->posts.ID) AS counter
		FROM $wpdb->posts 
			INNER JOIN $wpdb->term_relationships ON ($wpdb->posts.ID = $wpdb->term_relationships.object_id)
			INNER JOIN $wpdb->term_taxonomy ON ($wpdb->term_relationships.term_taxonomy_id = $wpdb->term_taxonomy.term_taxonomy_id)
			INNER JOIN $wpdb->postmeta ON ($wpdb->posts.ID = $wpdb->postmeta.post_id)
		WHERE
			$wpdb->posts.post_type = 'gh_lots'
			AND $wpdb->posts.post_status = 'publish'
			AND $wpdb->term_taxonomy.taxonomy = 'gh_lots_category'
			AND $wpdb->term_taxonomy.term_taxonomy_id = $term_id
			AND $wpdb->postmeta.meta_key = 'gh_lot_status'
			AND $wpdb->postmeta.meta_value = '".$status."'
		");

		return $count;
	}

	function gh_lot_category_title($term_id) {
		$title 	= '';
		$term 	= get_term_by('id', $term_id, 'gh_lots_category');
		
		if( $term && $term->parent !== 0 ) {
			$parent_term = get_term($term->parent, 'gh_lots_category');
			$title = ($parent_term ? $parent_term->name.' - ' : '');
		}

		$title .= $term->name;

		return $title;
	}

	function gh_lot_title_html($post_id) {
		$number = get_post_meta($post_id, 'gh_lot_number', true);
		$status = get_post_meta($post_id, 'gh_lot_status', true);
		$price 	= get_post_meta($post_id, 'gh_lot_price', true);
		$size 	= get_post_meta($post_id, 'gh_lot_size', true);
		
		$html = '<strong>'.$number.'</strong>';
		$html .= '<table>';
		$html .= '<tr><th>Status:</th><td>'.gh_lot_format_status($status).'</td></tr>';
		$html .= '<tr><th>Prijs:</th><td>'.gh_lot_format_price($price).'</td></tr>';
		$html .= '<tr><th>Oppervlakte:</th><td>'.gh_lot_format_size($size).'</td></tr>';
		$html .= '</table>';

		return $html;
	}

	function gh_lot_download_link($post_id, $default = '-') {
        $gh_lot_document 	= get_post_meta( $post_id, 'gh_lot_document', true );
        $gh_lot_number 	    = get_post_meta( $post_id, 'gh_lot_number', true );
		if( $gh_lot_document && $gh_lot_document !== '' ) {
			$gh_lot_download_url = wp_get_attachment_url($gh_lot_document);
			return '<a href="'.$gh_lot_download_url.'" aria-label="Download kavelpaspoort '.esc_attr($gh_lot_number).'" class="gh_lot--download_link"><img src="'.gh_lot_attachment_icon($gh_lot_document).'" alt="">Download</a>';
		}

		return $default;
	}

	function gh_lot_attachment_icon($attachment_id) {
		$base = GH_LOT_MODULE_URL . "/assets/images/icons/";
		$type = get_post_mime_type($attachment_id);
		switch ($type) {
			case 'image/jpeg':
				return $base . "jpg.png";
				break;
			case 'image/png':
				return $base . "png.png";
				break;
			case 'image/gif':
				return $base . "gif.png";
				break;
			case 'video/mpeg':
			case 'video/mp4': 
			case 'video/quicktime':
				return $base . "video.png";
				break;
			case 'text/xml':
				return $base . "xml.png";
				break;
			case 'text/plain': 
				return $base . "txt.png";
				break;
			case 'application/pdf':
				return $base . "pdf.png";
				break;
			default:
				return $base . "file.png";
				break;
		}
	}

	function gh_lot_format_status($status) {
		switch ($status) {
			case 'for_sale':
				return 'Te koop';
				break;
			case 'optional':
				return 'In optie';
				break;
			case 'sold':
				return 'Verkocht';
				break;
			default:
				return '-';
				break;
		}
	}

	function gh_lot_format_price($price) {
		return ($price !== '' ? '&euro; '.number_format(floatval($price), 2, ',', '.') : '-');
	}

	function gh_lot_format_size($size) {
		return ($size !== '' ? number_format(floatval($size), 0, ',', '.').' m²' : '-');
	}

	function gh_lot_color($post_id, $status = false) {
		$gh_lot_status 	= ($status ? $status : get_post_meta( $post_id, 'gh_lot_status', true ));
		
		switch ($gh_lot_status) {
			case 'for_sale':
				return 'green';
				break;
			case 'optional':
				return 'yellow';
				break;
			case 'sold':
				return 'red';
				break;
			default:
				return 'blue';
				break;
		}
	}

	function gh_lot_color_html($post_id, $status = false) {
		$color_html = '';
		$color 		= gh_lot_color($post_id, $status);

		switch ($color) {
			case 'red':
				$color_html = "{&quot;strokeColor&quot;:&quot;9c0000&quot;,&quot;fillColor&quot;:&quot;f00f00&quot;}";
				break;
			case 'green':
				$color_html = "{&quot;strokeColor&quot;:&quot;119210&quot;,&quot;fillColor&quot;:&quot;49ff48&quot;}";
				break;
			case 'yellow':
				$color_html = "{&quot;strokeColor&quot;:&quot;c7b31b&quot;,&quot;fillColor&quot;:&quot;ffe731&quot;}";
				break;
			default:
				$color_html = "{&quot;strokeColor&quot;:&quot;1080b3&quot;,&quot;fillColor&quot;:&quot;03A9F4&quot;}";
				break;
		}

		return "data-maphilight=\"".$color_html."\"";
	}

	function gh_lot_pagetype($term_id) {
		$term_meta = get_option( "taxonomy_term_$term_id" );
		
		return (isset($term_meta['gh_lot_page']) && $term_meta['gh_lot_page'] ? $term_meta['gh_lot_page'] : '');
	}