<?php
/*
* Copyright 2020 Gemeente Heerenveen
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
*/
	class GhlotInit {
		public function __construct() {
			add_action('admin_enqueue_scripts', array($this, 'admin_enqueue_scripts'));

			add_action('wp_enqueue_scripts', array($this, 'wp_enqueue_scripts'));

            add_action( 'pre_get_posts', array($this, 'custom_order') );
            
            add_action('pre_get_posts', array($this, 'pre_get_posts_enqueue_scripts'));
        }
        
        // Laad stylesheet en script enkel in op kavel pagina
        public function pre_get_posts_enqueue_scripts($query) {
            if( is_admin() ) {
                return $query;
            }
            
            $current_term = get_queried_object();
            if( isset($current_term->taxonomy) && $current_term->taxonomy == 'gh_lots_category' ){
                wp_enqueue_style('gh_lot_module_style');
                wp_enqueue_script('gh_lot_module_script');
            }
            return $query;
        }

		public function admin_enqueue_scripts() {
			wp_register_style('font-awesome', GH_LOT_MODULE_URL . '/assets/admin/css/font-awesome.min.css', false, GH_LOT_MODULE_VERSION);
			wp_register_style('gh_lot_module_style', GH_LOT_MODULE_URL . '/assets/admin/css/style.css', false, GH_LOT_MODULE_VERSION);
			wp_register_script('canvasAreaDraw', GH_LOT_MODULE_URL . '/assets/admin/js/jquery.canvasAreaDraw.min.js', array ('jquery'), GH_LOT_MODULE_VERSION, true);
			wp_register_script('panzoom', GH_LOT_MODULE_URL . '/assets/admin/js/jquery.panzoom.min.js', array ('jquery'), GH_LOT_MODULE_VERSION, true);
			wp_register_script('gh_lot_module_script', GH_LOT_MODULE_URL . '/assets/admin/js/base.min.js', array ('jquery', 'panzoom', 'canvasAreaDraw'), GH_LOT_MODULE_VERSION, true);
			
			wp_enqueue_style('font-awesome');
			wp_enqueue_style('gh_lot_module_style');
			wp_enqueue_script('gh_lot_module_script');
		}

		public function wp_enqueue_scripts() {
			wp_register_style('gh_lot_module_style', GH_LOT_MODULE_URL . '/assets/css/style.css', false, GH_LOT_MODULE_VERSION);
			wp_register_script('maphilight', GH_LOT_MODULE_URL . '/assets/js/jquery.maphilight.min.js', array ('jquery'), GH_LOT_MODULE_VERSION, true);
			wp_register_script('imageMapResizer', GH_LOT_MODULE_URL . '/assets/js/imageMapResizer.min.js', array ('jquery'), GH_LOT_MODULE_VERSION, true);
			wp_register_script('qtip', GH_LOT_MODULE_URL . '/assets/js/jquery.qtip.min.js', array ('jquery'), GH_LOT_MODULE_VERSION, true);
            wp_register_script('gh_lot_module_script', GH_LOT_MODULE_URL . '/assets/js/base.min.js', array ('jquery', 'maphilight', 'imageMapResizer', 'qtip'), GH_LOT_MODULE_VERSION, true);
		}

		public function custom_order($query) {
			if( is_admin() || $query->get('post_type') !== '' ) {
				return $query;
			}
			
			$current_term = get_queried_object();

			if( isset($current_term->taxonomy) && $current_term->taxonomy == 'gh_lots_category' ){
				$query->set('meta_key', 'gh_lot_number' );
				$query->set('orderby', 'meta_value_num meta_value');
				$query->set('order', 'ASC');
			}

			return $query;
		}
	}
	new GhlotInit();