<?php
/*
* Copyright 2020 Gemeente Heerenveen
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
*/
	class GhLotPosttype {
		public function __construct() {
			add_action( 'init', array($this, 'register_post_type') );
		}

		public function register_post_type() {
			$labels = array(
				'name'               => __( 'Kavels', 'gh-lot-module' ),
				'singular_name'      => __( 'Kavel', 'gh-lot-module' ),
				'menu_name'          => __( 'Kavels', 'gh-lot-module' ),
				'name_admin_bar'     => __( 'Kavels', 'gh-lot-module' ),
				'add_new'            => __( 'Toevoegen', 'gh-lot-module', 'gh-lot-module' ),
				'add_new_item'       => __( 'Nieuwe kavel toevoegen', 'gh-lot-module' ),
				'new_item'           => __( 'Nieuwe kavel', 'gh-lot-module' ),
				'edit_item'          => __( 'Bewerk kavel', 'gh-lot-module' ),
				'view_item'          => __( 'Bekijk kavel', 'gh-lot-module' ),
				'all_items'          => __( 'Alle kavels', 'gh-lot-module' ),
				'search_items'       => __( 'Zoek kavels', 'gh-lot-module' ),
				'parent_item_colon'  => __( 'Hoofdkavel:', 'gh-lot-module' ),
				'not_found'          => __( 'Geen kavels gevonden.', 'gh-lot-module' ),
				'not_found_in_trash' => __( 'Geen kavels gevonden in de prullenbak.', 'gh-lot-module' )
			);

			$args = array(
				'labels'             => $labels,
			    'description'        => __( 'Beschrijving.', 'gh-lot-module' ),
				'public'             => true,
				'publicly_queryable' => false,
				'show_ui'            => true,
				'show_in_menu'       => true,
				'query_var'          => true,
				'rewrite'            => array( 'slug' => 'kavels', 'with_front' => false ),
				'capability_type'    => 'post',
				'has_archive'        => false,
				'menu_icon'   		 => 'dashicons-location-alt',
				'hierarchical'       => false,
				'menu_position'      => null,
				'supports'           => array( 'title' ),
				'taxonomies'         => array( 'gh_lots_category' )
			);

			register_post_type( 'gh_lots', $args );

			register_taxonomy(
				'gh_lots_category',  //The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces).
				'gh_lots',   		 //post type name
				array(
					'hierarchical' 		=> true,
					'label' 			=> 'Kavel categorieën',  //Display name
					'query_var' 		=> true,
					'show_in_menu' 		=> true,
					// 'capabilities' 		=> array('manage_terms' => '', 'edit_terms' => '', 'delete_terms' => ''),
					'rewrite'			=> array(
							'slug' 			=> 'kavel', // This controls the base slug that will display before each term
							'with_front' 	=> true // Don't display the category base before
							)
					)
			);
		}
	}
	new GhLotPosttype();