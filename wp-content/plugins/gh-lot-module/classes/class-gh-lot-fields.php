<?php
/*
* Copyright 2020 Gemeente Heerenveen
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
*/
	class GhLotFields {
		public function __construct() {
			add_action( 'add_meta_boxes', array($this, 'add_meta_boxes') );

			add_action( 'save_post', array($this, 'save_post_fields'), 10, 2 );

			add_action( 'gh_lots_category_edit_form_fields', array($this, 'add_taxonomy_box'), 10, 1 );

			add_action( 'edited_gh_lots_category', array($this, 'save_taxonomy_fields'), 10, 1 ); 
		}

		public function add_meta_boxes() {
			add_meta_box( 'gh_lots_information', 'Informatie', array($this, 'post_information_meta_box'), 'gh_lots', 'normal', 'high' );
			add_meta_box( 'gh_lots_location', 'Locatie', array($this, 'post_location_meta_box'), 'gh_lots', 'normal', 'high' );
		}

		public function save_post_fields($post_id, $post) {
			if ($post->post_type == 'gh_lots'){
				if ( current_user_can('manage_options') ) {
					$gh_lot_location 	= (isset($_POST['gh_lot_location']) && $_POST['gh_lot_location'] ? $_POST['gh_lot_location'] : "");
					$gh_lot_status 		= (isset($_POST['gh_lot_status']) && $_POST['gh_lot_status'] ? $_POST['gh_lot_status'] : "");
					$gh_lot_number 		= (isset($_POST['gh_lot_number']) && $_POST['gh_lot_number'] ? $_POST['gh_lot_number'] : "");
					$gh_lot_size 		= (isset($_POST['gh_lot_size']) && $_POST['gh_lot_size'] ? $_POST['gh_lot_size'] : "");
					$gh_lot_price 		= (isset($_POST['gh_lot_price']) && $_POST['gh_lot_price'] ? $_POST['gh_lot_price'] : "");
					$gh_lot_document 	= (isset($_POST['gh_lot_document']) && $_POST['gh_lot_document'] ? intval($_POST['gh_lot_document']) : "");

					if( $gh_lot_location !== '' ) {
						update_post_meta( $post_id, 'gh_lot_location', $gh_lot_location );
					} else {
						delete_post_meta( $post_id, 'gh_lot_location' );
					}

					update_post_meta( $post_id, 'gh_lot_status', $gh_lot_status );
					update_post_meta( $post_id, 'gh_lot_number', $gh_lot_number );
					update_post_meta( $post_id, 'gh_lot_size', $gh_lot_size );
					update_post_meta( $post_id, 'gh_lot_price', $gh_lot_price );
					update_post_meta( $post_id, 'gh_lot_document', $gh_lot_document );
				}
			}
		}

		public function add_taxonomy_box($tag) {
			$t_id 				= $tag->term_id; 
			$term_meta 			= get_option( "taxonomy_term_$t_id" );
			$gh_lot_page 		= (isset($term_meta['gh_lot_page']) && $term_meta['gh_lot_page'] ? $term_meta['gh_lot_page'] : '');
			$attachment_id 		= (isset($term_meta['gh_lot_image']) && $term_meta['gh_lot_image'] ? $term_meta['gh_lot_image'] : '');
			$attachment_thumb 	= ($attachment_id !== '' ? wp_get_attachment_image_url( $attachment_id, 'thumbnail' ) : '');

			wp_enqueue_media();
			
			include GH_LOT_MODULE_DIR . '/views/admin/view-gh-lot-fields-taxonomy.php';
		}

		public function save_taxonomy_fields($term_id) {
			if ( isset( $_POST['term_meta'] ) ) {
				$term_meta 	= get_option( "taxonomy_term_$term_id" );  
				$cat_keys 	= array_keys( $_POST['term_meta'] );  
				
				foreach ( $cat_keys as $key ){  
					if ( isset( $_POST['term_meta'][$key] ) ){  
						$term_meta[$key] = $_POST['term_meta'][$key];  
					}
				}

				//save the option array  
				update_option( "taxonomy_term_$term_id", $term_meta );  
			}  
		}

		public function post_information_meta_box() {
			global $post;

			$gh_lot_status 		= get_post_meta( $post->ID, 'gh_lot_status', true );
			$gh_lot_number 		= get_post_meta( $post->ID, 'gh_lot_number', true );
			$gh_lot_size 		= get_post_meta( $post->ID, 'gh_lot_size', true );
			$gh_lot_price 		= get_post_meta( $post->ID, 'gh_lot_price', true );
			$gh_lot_document 	= get_post_meta( $post->ID, 'gh_lot_document', true );

			$attachment_thumb 	= ($gh_lot_document !== '' ? wp_get_attachment_image_url( $gh_lot_document, 'thumbnail', true ) : '');
			$attachment_title 	= ($gh_lot_document !== '' ? get_the_title($gh_lot_document) : '');

			wp_enqueue_media();

			include GH_LOT_MODULE_DIR . '/views/admin/view-gh-lot-fields-information.php';
		}

		public function post_location_meta_box() {
			global $post;
			$terms = get_the_terms($post, 'gh_lots_category');
			if( $terms ) {
				$term_id = $last_term_id = false;
				foreach ($terms as $term) {
					$last_term_id = $term->term_id;
					if( $term->parent > 0 ) {
						$term_id = $term->term_id;
					}
				}

				$term_id 			= ( $term_id ? $term_id : $last_term_id );
				$term_meta 			= get_option( "taxonomy_term_$term_id" );
				$attachment_id 		= (isset($term_meta['gh_lot_image']) && $term_meta['gh_lot_image'] ? $term_meta['gh_lot_image'] : '');
				$attachment_url 	= ($attachment_id !== '' ? wp_get_attachment_image_url( $attachment_id, 'full' ) : '');
				$gh_lot_location 	= get_post_meta( $post->ID, 'gh_lot_location', true );

				include GH_LOT_MODULE_DIR . '/views/admin/view-gh-lot-fields-location.php';
			} else {
				echo '<strong>'.__('Om een locatie aan te kunnen geven, dien je deze kavel eerst een categorie te geven.', 'gh-lot-module').'</strong>';
			}
		}
	}
	new GhLotFields();