<?php
/*
* Copyright 2020 Gemeente Heerenveen
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
*/
	class GhLotTemplates {
		public function __construct() {
			add_filter( 'single_template', array($this, 'load_single_template') );

			add_filter( 'template_include', array($this, 'load_taxonomy_template'), 100 );
		}

		public function load_single_template($template) {
			global $post;

			// Is this a "my-custom-post-type" post?
			if ($post->post_type == "gh_lots") {
				$plugin_template_dir = GH_LOT_MODULE_DIR . '/templates/';

				// The name of custom post type single template
				$template_name = 'single-gh_lots.php';

				// A specific single template for my custom post type exists in theme folder? Or it also doesn't exist in my plugin?
				if ($template === get_stylesheet_directory() . '/' . $template_name || !file_exists($plugin_template_dir . $template_name) ) {

				//Then return "single.php" or "single-my-custom-post-type.php" from theme directory.
				return $template;
				}

				// If not, return my plugin custom post type template.
				return $plugin_template_dir . $template_name;
			}

			//This is not my custom post type, do nothing with $template
			return $template;
		}

		public function load_taxonomy_template($template) {
			$taxonomy = get_query_var('taxonomy');

			if (strpos($taxonomy,'gh_lots_category') !== false) {
				$plugin_template_dir = GH_LOT_MODULE_DIR . '/templates/';

				// The name of custom taxonomy template
				$template_name = 'taxonomy-gh_lots_category.php';

				// A specific single template for my custom post type exists in theme folder? Or it also doesn't exist in my plugin?
				if ($template === get_stylesheet_directory() . '/' . $template_name || !file_exists($plugin_template_dir . $template_name) ) {
					//Then return "single.php" or "single-my-custom-post-type.php" from theme directory.
					return $template;
				} else if( file_exists(get_stylesheet_directory() . '/'. $template_name) ) {
					return get_stylesheet_directory() . '/'. $template_name;
				}

				// If not, return my plugin custom post type template.
				return $plugin_template_dir . $template_name;
			}
			return $template; 
		}
	}
	new GhLotTemplates();