/*
* Copyright 2020 Gemeente Heerenveen
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
*/
jQuery(function(){
	jQuery('img[usemap].gh_lot_map--img').maphilight({fillOpacity: 0.7});
	jQuery('#gh_lot_map').imageMapResize();
	jQuery('#gh_lot_map > area').qtip({
        style: {
            classes: 'qtip-gh_lot'
        },
        events: {
            show: function(event, api) {
                api.set({
                    'content.text': api.elements.target.attr('title')
                });
            }
        }
    });
});

jQuery(document).on('click', '.gh_lot_map--btn', function(e) {
	e.preventDefault();
	if( jQuery(this).hasClass('for_sale') ) {
		gh_lot_selected_toggle(jQuery(this), 'for_sale');
	} else if( jQuery(this).hasClass('optional') ) {
		gh_lot_selected_toggle(jQuery(this), 'optional');
	} else if( jQuery(this).hasClass('sold') ) {
		gh_lot_selected_toggle(jQuery(this), 'sold');
	}
});

jQuery(document).on('click', '.gh_lot_map--reset', function(e){
	e.preventDefault();
	jQuery('#gh_lot_price_filter').val('');
	jQuery('#gh_lot_size_filter').val('');
	jQuery('.gh_lot_map--btn.selected').removeClass('selected');
	gh_lot_filter_reset();
});

jQuery(document).on('click','#gh_lot_map area', function(e){
	e.preventDefault();
	var gh_lot_id 		= jQuery(this).data('unique-id');
	var gh_lot_anchor 	= jQuery("#gh_lot_anchor--"+gh_lot_id);
	if( gh_lot_anchor.length > 0 ) {
		if( jQuery(window).width() > 720 ) {
    		jQuery('html,body').animate({scrollTop: gh_lot_anchor.offset().top},'slow');
    	}

    	jQuery('.gh_lot_map--price_table tbody tr').removeClass('active');
    	gh_lot_anchor.addClass('active');
    }
});

jQuery(document).on('change', '#gh_lot_price_filter, #gh_lot_size_filter', function(e){
	gh_lot_filter_map();
});

function gh_lot_filter_map() {
	gh_lot_filter_reset();
	jQuery('.gh_lot_map--price_table tbody tr').removeClass('active');
	
	var gh_lot_filtered = false;
	var gh_lot_allowed 	= [];
	var gh_lot_elements = jQuery("#gh_lot_map area");
	var gh_lot_toggles 	= jQuery('.gh_lot_map--btn.selected');
	var gh_lot_price 	= jQuery('#gh_lot_price_filter').val();
	var gh_lot_size 	= jQuery('#gh_lot_size_filter').val();

	if( gh_lot_price !== '' ) {
		gh_lot_filtered = true;
		gh_lot_elements = gh_lot_elements.filter(function() {
		    return jQuery(this).attr("data-price") <= Number(gh_lot_price);
		});
	}

	if( gh_lot_size !== '' ) {
		gh_lot_filtered 	= true;
		var gh_lot_sizes 	= gh_lot_size.split('-');
		gh_lot_elements 	= gh_lot_elements.filter(function() {
			return jQuery(this).attr("data-size") > Number(gh_lot_sizes[0]);
		});

		if( gh_lot_sizes[1] ) {
			gh_lot_elements 	= gh_lot_elements.filter(function() {
				return jQuery(this).attr("data-size") < Number(gh_lot_sizes[1]);
			});
		}
	}

	if( gh_lot_toggles.length > 0 ) {
		gh_lot_filtered 	= true;
		gh_lot_toggles.each(function( index ) {
			if( jQuery(this).hasClass('for_sale') ) {
				gh_lot_allowed.push('.gh_lot--status_for_sale');
			} else if( jQuery(this).hasClass('optional') ) {
				gh_lot_allowed.push('.gh_lot--status_optional');
			} else if( jQuery(this).hasClass('sold') ) {
				gh_lot_allowed.push('.gh_lot--status_sold');
			}
		});

		gh_lot_allowed 	= gh_lot_allowed.join(', ');
		gh_lot_elements = gh_lot_elements.filter(function() {
			return jQuery(this).is(gh_lot_allowed);
		});
	}

	if( gh_lot_filtered ) {
		gh_lot_elements.each(function( index ) {
			var maphilight_data 		= jQuery(this).trigger('mouseout').data('maphilight') || {};
			maphilight_data.alwaysOn 	= true;

			jQuery(this).data('maphilight', maphilight_data).trigger('alwaysOn.maphilight');
		});
	}
}

function gh_lot_selected_toggle($gh_lot_el, $status) {
	var gh_lot_areas = jQuery('.gh_lot--status_'+$status);
	var gh_always_on = false;
	

	if( $gh_lot_el.hasClass('selected') ) {
		$gh_lot_el.removeClass('selected');
	} else {
		$gh_lot_el.addClass('selected');
	}

	gh_lot_filter_map();
}

function gh_lot_filter_reset() {
	jQuery('#gh_lot_map area').each(function( index ) {
		var maphilight_data 		= jQuery(this).trigger('mouseout').data('maphilight') || {};
		maphilight_data.alwaysOn 	= false;

		jQuery(this).data('maphilight', maphilight_data).trigger('alwaysOn.maphilight');
	});
}

function gh_lot_selected_reset() {
	jQuery('.gh_lot_map--btn').removeClass('selected');

	gh_lot_filter_reset();
}