jQuery(function() {
	var $gh_zoom_section 	= jQuery('#pan-when-zoomed');
	var $gh_zoom_elem 		= $gh_zoom_section.find('.panzoom');
	var $gh_drawing_elem 	= jQuery('.gh_drawing--canvas');

	$gh_drawing_elem.canvasAreaDraw();

	$gh_zoom_elem.panzoom({
		disablePan: true
	});

	jQuery('.gh_tool--drawing').addClass('active');

	jQuery('.gh_tool--moving').on('click', function(e) {
		e.preventDefault();
		
		jQuery('.gh_drawing--tools a').removeClass('active');
		jQuery('.gh_tool--moving').addClass('active');

		$gh_zoom_elem.panzoom("option", {disablePan:false});
	});

	jQuery('.gh_tool--drawing').on('click', function(e) {
		e.preventDefault();

		jQuery('.gh_drawing--tools a').removeClass('active');
		jQuery('.gh_tool--drawing').addClass('active');
		
		$gh_zoom_elem.panzoom("option", {disablePan:true});
	});

	jQuery('.gh_tool--zoom_in').on('click', function(e) {
		e.preventDefault();
		$gh_zoom_elem.panzoom("zoom");
	});

	jQuery('.gh_tool--zoom_out').on('click', function(e) {
		e.preventDefault();

		$gh_zoom_elem.panzoom("zoom", true);
	});

	jQuery('.gh_tool--reset').on("click", function(e){
		jQuery('.gh_drawing--tools a').removeClass('active');
		jQuery('.gh_tool--drawing').addClass('active');
		
		$gh_zoom_elem.panzoom("reset");
		$gh_zoom_elem.panzoom("option", {disablePan:true});

		jQuery('.gh_drawing--container .panzoom .btn').trigger('click');
	});
	// Uploading files
	var file_frame;
	jQuery('#upload_gh_lot_image_button').on('click', function( event ){
		event.preventDefault();
		// If the media frame already exists, reopen it.
		if ( file_frame ) {
			file_frame.open();
			return;
		}

		// Create the media frame.
		file_frame = wp.media.frames.file_frame = wp.media({
			title: 'Selecteer een bestand om te uploaden',
			button: {
				text: 'Gebruik dit bestand',
			},
			multiple: false	// Set to true to allow multiple files to be selected
		});
		// When an image is selected, run a callback.
		file_frame.on( 'select', function() {
			// We set multiple to false so only get one image from the uploader
			attachment = file_frame.state().get('selection').first().toJSON();
			// Do something with attachment.id and/or attachment.url here
			if( attachment.sizes == undefined || (attachment.sizes !== undefined && attachment.sizes.thumbnail === undefined ) ) {
				jQuery( '#gh_lot_image-preview' ).attr( 'src', attachment.icon ).attr('title', attachment.title).show();
			} else {
				jQuery( '#gh_lot_image-preview' ).attr( 'src', attachment.sizes.thumbnail.url ).show();
			}

			jQuery( '#gh_lot_image' ).val( attachment.id );
			
			jQuery( '#upload_gh_lot_image_button' ).hide();
			jQuery( '#upload_gh_lot_image_remove' ).show();
		});

		// Finally, open the modal
		file_frame.open();
	});
	// Restore the main ID when the add media button is pressed
	jQuery( '#upload_gh_lot_image_remove' ).on( 'click', function() {
		jQuery("#gh_lot_image-preview").attr('src', '').hide();
		jQuery( '#gh_lot_image' ).val('');

		jQuery( '#upload_gh_lot_image_button' ).show();
		jQuery( '#upload_gh_lot_image_remove' ).hide();
	});


	if( jQuery('#gh_lot_image').length > 0 ) {
		if( jQuery('#gh_lot_image').val() !== '' ) {
			jQuery("#gh_lot_image-preview").show();
			jQuery( '#upload_gh_lot_image_button' ).hide();
			jQuery( '#upload_gh_lot_image_remove' ).show();
		} else {
			jQuery("#gh_lot_image-preview").hide();
			jQuery( '#upload_gh_lot_image_button' ).show();
			jQuery( '#upload_gh_lot_image_remove' ).hide();
		}
	}
});