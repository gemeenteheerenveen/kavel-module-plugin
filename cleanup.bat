FOR /F "tokens=* USEBACKQ" %%F IN (`docker ps -a -q`) DO (
	docker stop %%F
	docker rm %%F
)

FOR /F "tokens=* USEBACKQ" %%F IN (`docker images -q`) DO (
	docker rmi -f %%F
)

if exist tmp (
   rmdir /S /Q tmp
)

if exist wp-content\uploads (
   rmdir /S /Q wp-content\uploads
)

rem for /R "wp-content\plugins\" %%G in (*.*) DO (
rem     echo %%~nxG
rem )

pause