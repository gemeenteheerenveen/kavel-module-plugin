# Kavel module

**Requires at least:** WordPress 4.4
**Tested up to:** WordPress 4.7
**Stable tag:** 1.0.20
**Version:** 1.0.20

## Changelog

### 1.0.3: 30 Mei 2018
- Gulp release functie toegevoegd
- Update class toegevoegd

### 1.0.2: 09 Mei 2018
- Bug fix: "sourceMappingURL={naam van min.js}.map" weggehaald bij imageMapResizer.min.js en jquery.qtip.min.js
  (De code moest verwijderd ipv overgeslagen worden)

### 1.0.1: 26 Februari 2018
- Bug fix: tijdens het laden van een menu op de kavel pagina werd de order hook geladen.

### 1.0.0: 9 Augustus 2017
- Initial commit