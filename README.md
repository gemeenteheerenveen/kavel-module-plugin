# Kavel module (WP)
  
Hieronder staan alle verschillende elementen beschreven om te kunnen beginnen met de doorontwikkelingen van de Kavel module

## Overzicht

  * [Vereisten](#markdown-header-vereisten)
  * [Installatie](#markdown-header-installatie)
  * [Versiebeheer](#markdown-header-versiebeheer)
  * [Commando's voor Docker](#markdown-header-commandos-voor-docker)
  * [Changelog](#markdown-header-changelog)
  * [Vragen?](#markdown-header-vragen)
  
  
## Vereisten
Voor de doorontwikkeling heb je het programma Docker nodig, deze is via de volgende link te downloaden: [Docker](https://www.docker.com/community-edition).
Nadat je de setup doorlopen hebt, kun je beginnen met de [installatie](##markdown-header-installatie) van de testomgeving.

## Installatie
1. Download de meeste recente versie van de plugin
2. Open je Terminal(Mac OS) of CMD(Windows)
3. Navigeer naar de map waar je zojuist de plugin hebt geplaatst
4. Voer de volgende command uit: `docker-compose up --build`
5. Na het uitvoeren van deze command kun je in je browser naar [localhost](http://localhost/) (127.0.0.1) navigeren

## Commando's voor Docker
Activeer de docker omgeving en installeer de benodigde pakketten
`docker-compose up --build`

Activeer de docker omgeving
`docker-compose up`

Verwijder de docker omgeving (vergeet niet de tmp map te verwijderen om zo de database te resetten)
`docker-compose rm -v`
  
## Versiebeheer

## Changelog
Wijzigingen binnen de plugin worden bijgehouden in de [CHANGELOG.md](CHANGELOG.md).

## Vragen?
Voor eventuele vragen of onduidelijkheden kun je contact opnemen met één van de onderstaande personen:
#### Gemeente Heerenveen
1. [Jelle Hoekstra](mailto:j.hoekstra@heerenveen.nl) (projectleider)
2. [Stefan Woudstra](mailto:s.woudstra@heerenveen.nl) (programmeur)
3. [Astrid Wielinga](mailto:a.wielinga@heerenveen.nl) (webredacteur)
4. [Jessica Derks-Dekker](mailto:j.derks@heerenveen.nl) (webredacteur)
