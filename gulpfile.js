const project_location = 'plugins/gh-lot-module';
const project_filename = 'gh-lot-module.php';
const function_vers_var = 'GH_LOT_MODULE_VERSION';

const argv = require('minimist')(process.argv.slice(2)),
    sass = require('gulp-sass')(require('node-sass')),
    less = require('gulp-less'),
    rtlcss = require('gulp-rtlcss'),
    autoprefixer = require('gulp-autoprefixer'),
    plumber = require('gulp-plumber'),
    { series, parallel, src, dest, watch } = require('gulp'),
    gutil = require('gulp-util'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin'),
    zip = require('gulp-zip'),
    header = require('gulp-header'),
    merge = require('merge-stream'),
    del = require('del');

// sass.compiler = require('sass'); // Actuele SASS, maar onze SCSS is nog niet compatibel

const onError = function (err) {
    console.log('An error occurred:', gutil.colors.magenta(err.message));
    gutil.beep();
    this.emit('end');
};

// Grab the package.json file for the version
const getPackageJson = function () {
    const fs = require('fs');

    return JSON.parse(fs.readFileSync('package.json', 'utf8'));
};

// Sass
exports.sass = function (cb) {
    return src('./wp-content/' + project_location + '/assets/sass/**/style.scss')
        .pipe(plumber({ errorHandler: onError }))
        .pipe(sass())
        .pipe(autoprefixer())
        .pipe(dest('./wp-content/' + project_location + '/assets/css/'))
        .pipe(rtlcss())                     // Convert to RTL
        .pipe(rename({ basename: 'rtl' }))  // Rename to rtl.css
        .pipe(dest('./wp-content/' + project_location + '/assets/css/'));             // Output RTL stylesheets (rtl.css)
};

// Less
exports.less = function (cb) {
    return src('./wp-content/' + project_location + '/assets/less/style.less', { allowEmpty: true })
        .pipe(less())
        .pipe(autoprefixer())
        .pipe(dest('./wp-content/' + project_location + '/assets/css/'))
        .pipe(rtlcss())                     // Convert to RTL
        .pipe(rename({ basename: 'rtl' }))  // Rename to rtl.css
        .pipe(dest('./wp-content/' + project_location + '/assets/css/'));
};

// JavaScript
exports.js = function (cb) {
    const merged = new merge();
    merged.add(
        src(['./wp-content/' + project_location + '/assets/js/base.js'])
        .pipe(jshint())
        .pipe(jshint.reporter('default'))
        .pipe(rename({ suffix: '.min' }))
        .pipe(uglify())
        .pipe(dest('./wp-content/' + project_location + '/assets/js'))
    );
    merged.add(
        src([
            './wp-content/' + project_location + '/assets/admin/js/base.js',
            './wp-content/' + project_location + '/assets/admin/js/jquery.canvasAreaDraw.js',
            './wp-content/' + project_location + '/assets/admin/js/jquery.panzoom.js',
        ])
        .pipe(jshint())
        .pipe(jshint.reporter('default'))
        .pipe(rename({ suffix: '.min' }))
        .pipe(uglify())
        .pipe(dest('./wp-content/' + project_location + '/assets/admin/js'))
    );
    return merged.on('end', cb);
};

// Images
exports.images = function (cb) {
    return src('./wp-content/' + project_location + '/assets/images/src/*')
        .pipe(plumber({ errorHandler: onError }))
        .pipe(imagemin({ optimizationLevel: 7, progressive: true }))
        .pipe(dest('./wp-content/' + project_location + '/assets/images/dist'));
};

// Watch
exports.watch = parallel(
    () => { watch('./wp-content/' + project_location + '/assets/sass/**/*.scss', exports.sass) },
    () => { watch('./wp-content/' + project_location + '/assets/less/**/*.less', exports.less) },
    () => { watch('./wp-content/' + project_location + '/assets/images/src/*', exports.images) },
);

exports.build = parallel(
    exports.sass,
    exports.less,
    exports.images,
    exports.js
);
exports.default = parallel(
    exports.watch
)

// Our versioning
exports['build-release'] = function (cb) {
    // get the version which we want to bump up to.  
    var version = argv.type;
    var noupdate = argv.noupdate; // --noupdate

    if (typeof (argv.type) === 'undefined') {
        version = 'patch'; // major.minor.patch (https://semver.org/)
    }

    if (typeof (argv.noupdate) === 'undefined') {
        noupdate = false;
    } else {
        noupdate = true;
    }

    // require some packages that we need
    const bump = require('gulp-bump');
    const semver = require('semver');
    const replace = require('gulp-replace');

    // reget package
    const pkg = getPackageJson();
    let newVer;

    if (noupdate) {
        newVer = pkg.version;
    } else {
        // increment version
        newVer = semver.inc(pkg.version, version);
    }

    const src_path = 'wp-content/' + project_location;
    const merged = merge();

    // files we want to transfer for release
    merged.add(
        src('package.json')
            .pipe(bump({
                version: newVer,
                type: version
            }))
            .pipe(dest('./releases/' + newVer + '/' + pkg.name))
            .pipe(dest('./'))
    );

    // Move the styles.css into the new folder
    //   merged.add(
    //       src( src_path + '/style.css')
    //       // perform the version up in the style.css file
    //         .pipe(replace(/Version:[ ]+[0-9].+/g, 'Version: '+newVer))
    //         .pipe(dest('./releases/'+newVer + '/'+pkg.name))
    //         .pipe(dest( src_path + '/' ))
    //   );

    const regex = new RegExp("\'" + function_vers_var + "\', \'+[0-9].+\'", "g");

    // Replace the functions.php version
    merged.add(
        src(src_path + '/' + project_filename)
            .pipe(replace(regex, '\'' + function_vers_var + '\', \'' + newVer + '\''))
            .pipe(replace(/Version:[ ]+[0-9].+/g, 'Version: ' + newVer))
            .pipe(dest(src_path + '/'))
            .pipe(dest('./releases/' + newVer + '/' + pkg.name))
    );

    merged.add(
        src('CHANGELOG.md')
            .pipe(header(
                "# " + pkg.description + "\n\n" +
                "**Requires at least:** WordPress 4.4\n" +
                "**Tested up to:** WordPress 4.7\n" +
                "**Stable tag:** " + newVer + "\n" +
                "**Version:** " + newVer + "\n\n" +
                "## Changelog\n\n"
            ))
            .pipe(rename("readme.txt"))
            .pipe(dest('./'))
    );

    const pluginInfo = {
        "name": "Kavel module",
        "version": newVer,
        "download_url": "https://bitbucket.org/gemeenteheerenveen/kavel-module-plugin/downloads/gh-lot-module-" + newVer + ".zip",
        "slug": "gh-lot-module",
        "requires": "5.0",
        "tested": "5.3",
        "author_homepage": "https://www.heerenveen.nl",
        "author": "Gemeente Heerenveen",
        "sections": {
            "description": "Plugin voor het weergeven van verschillende kavels"
        }
    };
    require('fs').writeFileSync(__dirname + '/dist/update.json', JSON.stringify(pluginInfo, null, 2));

    merged.add(
        src('LICENSE.txt')
            .pipe(dest('./releases/' + newVer + '/' + pkg.name))
    );

    // move package files for deployment
    merged.add(
        src([
            src_path + '/screenshot.*',
            src_path + '/**/*.php',
            src_path + '/**/*.mo',
            src_path + '/**/*.css',
            src_path + '/**/*.js',
            src_path + '/**/*.svg',
            src_path + '/**/*.png',
            src_path + '/**/*.jpg',
            src_path + '/**/*.jpeg',
            src_path + '/**/*.gif',
            '!' + src_path + '/assets/svg/*.svg',
            '!' + src_path + '/assets/images/src/*',
            '!' + src_path + '/assets/less/*',
            '!' + src_path + '/assets/sass/*',
            '!' + src_path + '/functions.php',
        ], { base: './' + src_path }
        ).pipe(dest('./releases/' + newVer + '/' + pkg.name))
    );
    return merged.on('end', cb);
};

exports['zip-release'] = function (cb) {
    // reget package
    const pkg = getPackageJson();
    const merged = merge();
    merged.add(
        src('./releases/' + pkg.version + '/**')
            .pipe(zip('gh-lot-module-latest.zip'))
            .pipe(dest('dist')),
    );
    merged.add(
        src('./releases/' + pkg.version + '/**')
            .pipe(zip('gh-lot-module-' + pkg.version + '.zip'))
            .pipe(dest('dist'))
    );

    // zip the release
    return merged.on('end', cb);
};

// Our versioning
exports.release = series(
    exports['build'],
    exports['build-release'],
    exports['zip-release']
);